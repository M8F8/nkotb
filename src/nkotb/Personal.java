package nkotb;

import java.util.Scanner;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Personal {

    PreparedStatement stmt = null;
    ResultSet rs = null;
    Scanner input = new Scanner(System.in); 
    
    public void PersonalTransfer() {

        try {
            Connect conObj = new Connect();
            conObj.Connection();
            stmt = conObj.Connection().prepareStatement("SELECT * FROM Accounts");
            rs = stmt.executeQuery();
            
            while (rs.next()){
            float dbPersonal = rs.getFloat("Personal");
            
                System.out.println("Personal account funds: " + dbPersonal);
            }
            
        } catch (SQLException exc) {
            System.out.println(exc.getMessage());
        }
    }

}
