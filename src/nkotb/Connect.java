package nkotb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {

    String url = "jdbc:mysql://localhost:3306/nkotb";
    String user = "root";
    String password = "1337";
    Connection conn = null;

    //Denna metod används för att ansluta till databsen istället för att göra variabler för url, user och password i varje metod
    public Connection Connection() {
        try {
            //1. Anslutning till mysql
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException exc) {
            System.out.println(exc.getMessage());
        }
        return conn;
    }
}
