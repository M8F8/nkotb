package nkotb;

import java.util.Scanner;
import java.sql.*;
import java.sql.PreparedStatement;

public class Bank {

    public static void main(String[] args) {

        //Anropar Loginklassen
        Login loginObj = new Login();
        loginObj.Login();

        System.out.println("1. Overview.");
        System.out.println("2. Deposit.");
        System.out.println("3. Transfer");

        Scanner input = new Scanner(System.in);
        int mainMenu = input.nextInt();

        if (mainMenu == 1) {
            Accounts overviewObject = new Accounts();
            overviewObject.Accounts();
        } else if (mainMenu == 2) {
            Deposit depositObj = new Deposit();
            depositObj.Deposit();
        } else if (mainMenu == 3) {
            Transfer transfObj = new Transfer();
            transfObj.OwnTransfer();

        }

    }
}
