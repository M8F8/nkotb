package nkotb;

import java.util.Scanner;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Accounts {

    
    PreparedStatement stmt = null;
    ResultSet rs = null;
    Scanner userInput = new Scanner(System.in);

    public void Accounts() {

        System.out.println("Enter your ID.");
        String Id = userInput.next();

        try {
            Connect conObj = new Connect();
            conObj.Connection();
            stmt = conObj.Connection().prepareStatement("SELECT * FROM Accounts WHERE ID ='" + Id + "' ; ");
            rs = stmt.executeQuery();

            while (rs.next()) {
                String dbLoan = rs.getString("Loans");
                String dbSavings = rs.getString("Savings");

                System.out.println("Loans & Savings:");
                System.out.println("Loans: " + dbLoan);
                System.out.println("Savings: " + dbSavings);
            }

        } catch (SQLException exc) {
            System.out.println(exc.getMessage());
        }
    }

}
